package lobbysystem;

import lobbysystem.listener.*;
import lobbysystem.setup.Commands;
import lobbysystem.utils.ItemStorage;
import lobbysystem.utils.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class LobbySystem extends JavaPlugin {

    private static LobbySystem instance;
    private final String prefix = "§7[§eLobby§7] ";
    private final ItemStorage itemStorage = new ItemStorage();
    private final WarpManager warpManager = new WarpManager();
    private final ArrayList<Player> hidePlayers = new ArrayList<>();

    @Override
    public void onEnable() {
        instance = this;
        register();

        getItemStorage().loadItems();
        try {
            getWarpManager().loadDataFromFileIntoTempStorage();
        } catch (Exception e) {
            getLogger().info("Could not load warps.");
            e.printStackTrace();
        }
    }

    /**
     * Returns the instance
     * @return
     */

    public static LobbySystem getInstance(){
        return instance;
    }

    /**
     * Register all events / commands
     */

    private void register(){

        final PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new PlayerJoinListener(), this);
        pluginManager.registerEvents(new PluginDisableListener(), this);
        pluginManager.registerEvents(new PlayerInteractListener(), this);
        pluginManager.registerEvents(new PlayerJoinListener(), this);
        pluginManager.registerEvents(new InventoryClickListener(), this);
        pluginManager.registerEvents(new GameProtectionListener(), this);

        getCommand("setup").setExecutor(new Commands());

    }

    /**
     * Returns the itemStorage class with saved items
     * @return
     */

    public ItemStorage getItemStorage() {
        return itemStorage;
    }

    /**
     * Returns the prefix
     * @return
     */

    public String getPrefix() {
        return prefix;
    }

    /**
     * Class for handle warps
     * @return
     */

    public WarpManager getWarpManager() {
        return warpManager;
    }

    public ArrayList<Player> getHidePlayers() {
        return hidePlayers;
    }
}
