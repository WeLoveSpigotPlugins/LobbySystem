package lobbysystem.listener;

import lobbysystem.LobbySystem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event){
        final Player player = event.getPlayer();

        if(LobbySystem.getInstance().getWarpManager().existsWarpInCache("Lobby")){
            player.teleport(LobbySystem.getInstance().getWarpManager().getLocation("Lobby"));
        } else {
            if(player.hasPermission("lobbysystem.admin")){
                player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du musst den §eLobby Spawn §7mit §b/Setup SetSpawn Lobby §7setzen§8.");
            }
        }

        event.setJoinMessage(null);
        getLobbyItems(player);

        LobbySystem.getInstance().getHidePlayers().forEach(entry ->{
            entry.hidePlayer(player);
        });

    }

    private void getLobbyItems(final Player player){

        player.getInventory().clear();
        player.getInventory().setArmorContents(null);

        player.getInventory().setItem(0, LobbySystem.getInstance().getItemStorage().getCompass());
        player.getInventory().setItem(4, LobbySystem.getInstance().getItemStorage().getPlayerhider());
        player.getInventory().setItem(8, LobbySystem.getInstance().getItemStorage().getLobbyswitche());

    }

}
