package lobbysystem.listener;


import lobbysystem.LobbySystem;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */

public class PluginDisableListener implements Listener {

    @EventHandler
    public void onPluginDisable(final PluginDisableEvent event){
        LobbySystem.getInstance().getWarpManager().saveTempDataIntoFile();
    }
}
