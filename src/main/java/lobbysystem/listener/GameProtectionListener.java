package lobbysystem.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class GameProtectionListener implements Listener {

    @EventHandler
    public void onBreak(final BlockBreakEvent event){
        if(!event.getPlayer().hasPermission("lobbysystem.admin") && event.getPlayer().getGameMode() == GameMode.CREATIVE){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(final BlockPlaceEvent event){
        if(!event.getPlayer().hasPermission("lobbysystem.admin") && event.getPlayer().getGameMode() == GameMode.CREATIVE){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(final PlayerDropItemEvent event){
        if(event.getPlayer().getGameMode() != GameMode.CREATIVE){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClick(final InventoryClickEvent event){
        final Player player = (Player) event.getWhoClicked();
        if(player.getGameMode() != GameMode.CREATIVE){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onClick(final FoodLevelChangeEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onEntity(final EntityDamageEvent event){
        event.setCancelled(true);
    }
}
