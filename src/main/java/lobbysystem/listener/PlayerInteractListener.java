package lobbysystem.listener;

import lobbysystem.LobbySystem;
import lobbysystem.utils.ItemStorage;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(final PlayerInteractEvent event){
        if(event.getPlayer() == null){
            return;
        }

        final Player player = event.getPlayer();

        if(event.getItem() == null) {
            return;
        }

        if(event.getItem().getItemMeta().getDisplayName() == null){
            return;
        }

        if(event.getAction().equals(Action.LEFT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK)){
            return;
        }

        final ItemStorage itemStorage = LobbySystem.getInstance().getItemStorage();

        if(event.getItem().getItemMeta().getDisplayName().equals(itemStorage.getCompass().getItemMeta().getDisplayName())){
            openCompass(player, itemStorage);
        }

        if(event.getItem().getItemMeta().getDisplayName().equals(itemStorage.getPlayerhider().getItemMeta().getDisplayName())){
            openPlayerHiderInventory(player, itemStorage);
        }

        if(event.getItem().getItemMeta().getDisplayName().equals(itemStorage.getLobbyswitche().getItemMeta().getDisplayName())){
            openLobbySwitcherInventory(player, itemStorage);
        }

    }

    /**
     * Opens the compass for the player
     * @param player
     * @param itemStorage
     */

    private void openCompass(final Player player, final ItemStorage itemStorage){

        final Inventory inventory = Bukkit.createInventory(null, 27, "§eNavigator");

        inventory.setItem(4, itemStorage.getBedwars());
        inventory.setItem(11, itemStorage.getFfa());
        inventory.setItem(15, itemStorage.getKnockbackffa());
        inventory.setItem(22, itemStorage.getSkywars());

        player.playSound(player.getLocation(), Sound.LEVEL_UP, 10F, 10F);
        player.openInventory(inventory);

    }

    /**
     * Opens the playehider for the player
     * @param player
     * @param itemStorage
     */

    private void openPlayerHiderInventory(final Player player, final ItemStorage itemStorage) {

        final Inventory inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "§ePlayerhider");

        inventory.setItem(0, itemStorage.getShowPlayers());
        inventory.setItem(4, itemStorage.getHidePlayers());

        player.playSound(player.getLocation(), Sound.LEVEL_UP, 10F, 10F);
        player.openInventory(inventory);

    }

    /**
     * Opens the lobbyswitcher for the player
     * @param player
     * @param itemStorage
     */

    private void openLobbySwitcherInventory(final Player player, final ItemStorage itemStorage) {

        final Inventory inventory = Bukkit.createInventory(null, InventoryType.HOPPER, "§eLobbySwitcher");

        for(int i = 1 ; i < 6 ; i++){
            inventory.setItem(i -1, itemStorage.getLobbyServerState(i));
        }

        player.playSound(player.getLocation(), Sound.LEVEL_UP, 10F, 10F);
        player.openInventory(inventory);
    }

}
