package lobbysystem.listener;

import lobbysystem.LobbySystem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class InventoryClickListener implements Listener{

    @EventHandler
    public void onClick(final InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }

        final Player player = (Player) event.getWhoClicked();

        if (event.getInventory() == null) return;

        if (event.getCurrentItem() == null) return;

        if (event.getCurrentItem().getItemMeta() == null) return;

        if(event.getCurrentItem().getItemMeta().getDisplayName() == null) return;

        if (event.getInventory().getName().equals("§eNavigator")) {

            event.setCancelled(true);

            final String currentItem = event.getCurrentItem().getItemMeta().getDisplayName();

            if (LobbySystem.getInstance().getWarpManager().existsWarpInCache(ChatColor.stripColor(currentItem))) {

                player.teleport(LobbySystem.getInstance().getWarpManager().getLocation(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName())));
                player.playEffect(player.getLocation(), Effect.POTION_BREAK, 1);
                player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du hast dich zu " + event.getCurrentItem().getItemMeta().getDisplayName() + "§7 teleportiert§8.");
                return;

            } else {

                player.sendMessage(LobbySystem.getInstance().getPrefix()  + "§7Du kannst dich aktuell nicht zum Warp §e" + currentItem + "§7 teleportieren§8.");

            }
            return;
        }

        if(event.getInventory().getName().equals("§ePlayerhider")){

            if(event.getCurrentItem().getItemMeta().getDisplayName().equals(LobbySystem.getInstance().getItemStorage().getShowPlayers().getItemMeta().getDisplayName())){
                Bukkit.getOnlinePlayers().forEach(player::hidePlayer);
                player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du §asiehst §7nun alle Spieler§8.");
                player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
                player.closeInventory();
                LobbySystem.getInstance().getHidePlayers().add(player);
                return;
            }

            if(event.getCurrentItem().getItemMeta().getDisplayName().equals(LobbySystem.getInstance().getItemStorage().getHidePlayers().getItemMeta().getDisplayName())){
                Bukkit.getOnlinePlayers().forEach(player::showPlayer);
                player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du hast alle Spieler §cversteckt§8.");
                player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
                player.closeInventory();
                LobbySystem.getInstance().getHidePlayers().remove(player);
                return;
            }

        }

    }
}
