package lobbysystem.setup;

import lobbysystem.LobbySystem;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class Commands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)){
            return true;
        }

        final Player player = (Player) sender;

        if(!player.hasPermission("lobbysystem.admin")){
            player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du hast keine Rechte, um dies zu tun§8.");
            return true;
        }

        if(args.length != 2){
            player.sendMessage(LobbySystem.getInstance().getPrefix() + "§e/Setup SetSpawn [Spawn]");
            return true;
        }

        if(args[0].equals("setspawn")){

            LobbySystem.getInstance().getWarpManager().addNewWarp(args[1], player.getLocation());
            player.sendMessage(LobbySystem.getInstance().getPrefix() + "§7Du hast den Spawn §e" + args[1] + "§7 gesetzt§8.");
            player.playSound(player.getLocation(), Sound.LEVEL_UP, 10F, 10F);

        } else {
            player.sendMessage(LobbySystem.getInstance().getPrefix() + "§e/Setup SetSpawn [Spawn]");
            return true;
        }


        return false;
    }
}
