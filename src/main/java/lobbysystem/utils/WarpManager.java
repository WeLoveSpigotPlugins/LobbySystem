package lobbysystem.utils;

import lobbysystem.LobbySystem;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class WarpManager {

    private final File file = new File("plugins//LobbySystem//warps.yml");
    private final YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(this.file);

    private final List<String> warpNames = new ArrayList<>();
    private final Map<String, Location> locationMap = new HashMap<>();

    /**
     * Add new warp into temporaly cache.
     * @param warpName
     * @param warpLocation
     */

    public void addNewWarp(String warpName, Location warpLocation){
        this.warpNames.add(warpName);
        this.locationMap.put(warpName, warpLocation);
    }

    /**
     * Saves all temporaly data into file
     */

    public void saveTempDataIntoFile(){
        for(final String string : warpNames){
            this.yamlConfiguration.set("Warp." + string, locationMap.get(string));
        }

        this.yamlConfiguration.set("Warps", warpNames);

        try {
            this.yamlConfiguration.save(this.file);
        } catch (IOException e) {
            LobbySystem.getInstance().getLogger().info("Failed to save warps into file");
            e.printStackTrace();
        }

    }

    /**
     * Returns a location from temporaly cache.
     * @param warpName
     * @return
     */

    public Location getLocation(final String warpName){
        return locationMap.get(warpName);
    }

    /**
     * Returns if a warp exists in temporal cache or not.
     * @param warpName
     * @return
     */

    public boolean existsWarpInCache(final String warpName){
        return locationMap.containsKey(warpName);
    }

    public void loadDataFromFileIntoTempStorage() throws Exception{

        final List<String> warps = yamlConfiguration.getStringList("Warps");

        if(warps.isEmpty()) {
            LobbySystem.getInstance().getLogger().info("No maps could be loaded yet.");
            return;
        }

        for(final String string : warps){
            warpNames.add(string);
            locationMap.put(string, (Location) yamlConfiguration.get("Warp." + string));
            LobbySystem.getInstance().getLogger().info("Loaded warp " + string);
        }

        LobbySystem.getInstance().getLogger().info("Successfully loaded all warps");

    }

}
