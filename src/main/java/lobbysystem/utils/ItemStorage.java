package lobbysystem.utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Pascal Falk on 29.12.2017.
 * Plugin by WeLoveSpigotPlugins
 * https://youtube.com/welovespigotplugins
 * Coded with IntelliJ
 */
public class ItemStorage {

    /**
     * Player hotbar Items
     */

    private ItemStack compass;
    private ItemStack playerhider;
    private ItemStack lobbyswitche;

    /**
     * Compass items
     */

    private ItemStack bedwars;
    private ItemStack skywars;
    private ItemStack ffa;
    private ItemStack knockbackffa;

    /**
     *  Playerhider items
     */

    private ItemStack showPlayers;
    private ItemStack hidePlayers;

    public void loadItems(){

        /**
         * Building the players hotbar items
         */

        compass = new ItemManager(Material.COMPASS).setDisplayName("§eNavigator").build();
        playerhider = new ItemManager(Material.BLAZE_ROD).setDisplayName("§ePlayerhider").build();
        lobbyswitche = new ItemManager(Material.NETHER_STAR).setDisplayName("§eLobbySwitcher").build();

        /**
         * Building the compass items
         */

        bedwars = new ItemManager(Material.BED).setDisplayName("§bBedWars").addLoreAll(getList("§bBedWars")).build();
        skywars = new ItemManager(Material.GRASS).setDisplayName("§5SkyWars").addLoreAll(getList("§5SkyWars")).build();
        ffa = new ItemManager(Material.FISHING_ROD).setDisplayName("§9FFA").addLoreAll(getList("§9FFA")).build();
        knockbackffa = new ItemManager(Material.STICK)
                .addEnchantment(Enchantment.KNOCKBACK, 1)
                .addItemFlag(ItemFlag.HIDE_ENCHANTS)
                .setDisplayName("§eKnockbackFFA")
                .addLoreAll(getList("§eKnockbackFFA"))
                .build();

        /**
         * Building the playerhider items
         */

        showPlayers = new ItemManager(Material.INK_SACK).setData((short) 10).setDisplayName("§aSpieler anzeigen").build();
        hidePlayers = new ItemManager(Material.INK_SACK).setData((short) 1).setDisplayName("§cSpieler verstecken").build();

        /**
         * Building the lobbyswitchers items
         */

    }

    public ItemStack getCompass() {
        return compass;
    }

    public ItemStack getPlayerhider() {
        return playerhider;
    }

    public ItemStack getLobbyswitche() {
        return lobbyswitche;
    }

    private List<String> getList(String compassName){
        return Arrays.asList("", "§8§m----------------", "§eTeleport to " + compassName, "§8§m----------------");
    }

    public ItemStack getBedwars() {
        return bedwars;
    }

    public ItemStack getSkywars() {
        return skywars;
    }

    public ItemStack getFfa() {
        return ffa;
    }

    public ItemStack getKnockbackffa() {
        return knockbackffa;
    }

    public ItemStack getShowPlayers() {
        return showPlayers;
    }

    public ItemStack getHidePlayers() {
        return hidePlayers;
    }

    public ItemStack getLobbyServerState(int lobby){

        ItemStack itemStack;

        // =============================================//
        //             REQUIRES OWN API                 //
        //             if(Server.isOnline(number...){}  //
        // =============================================//

        //  TEMPORALY ->

        return new ItemManager(Material.BARRIER)
                .setDisplayName("§bLobby " + lobby + " Offline")
                .addLoreLine("")
                .addLoreLine("§8§m---------------------")
                .addLoreLine("§7Lobby " + lobby + "§c Offline")
                .addLoreLine("§8§m---------------------")
                .build();


    }

}
